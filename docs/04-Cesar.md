---
hide:
  - navigation
---

# Chiffrement de César

Il s'agit d'une des plus anciennes techniques de chiffrement décrite à ce jour. Elle utilise la **substitution mono-alphabétique**: chaque lettre de l'alphabet est remplacée (*substituée*) par une seule autre lettre (*mono-alphabétique*) du même alphabet, obtenu par **décalage** (=*clé*).

<figure markdown>
![Roue de César](img/05-roue_cesar.svg)
<figcaption>Chiffrement de César avec un décalage de 3 caractères vers la gauche</figcaption>
</figure>

> Jules César aurait eu pour habitude d'utiliser un décalage de 3 lettres vers la gauche (*d* donne *a*, *e* donne *b*, *f* donne *c*...), mais il est possible d'utiliser n'importe quelle autre valeur de décalage.

## 1. Alphabet de travail

La roue précédente est limitée à 26 lettres minuscules, mais on peut incorporer d'autres caractères (majuscules, symboles, caractères accentués, émojis...) en se basant sur l'ordre et les codes de tables normalisées (ASCII, unicode...).

<figure markdown>
![ASCII](img/05-ascii.svg)
<figcaption>Table de correspondance ASCII<br><small>(`a` a pour code `97`, `A` vaut `65`...)</small></figcaption>
</figure>

??? tip "Codage des caractères en Python"

    - La fonction `#!py3 ord()` donne le code d'un caractère :
    ```python
    ord('a')
    ```

    - La fonction `#!py3 chr()` donne le caractère correspondant à un code :
    ```python
    chr(97)
    ```

L'ensemble des codes (code<sub>min</sub> à code<sub>max</sub>) utilisés pour le chiffrement/déchiffrement définit l'**alphabet de travail**.

> La roue de Jules César n'utilise que les codes de `97` à `122` mais on voit que, dans la table ASCII, il y a d'autres caractères affichables (par exemple de `32` à `126`)

## 2. Chiffrement
    
Le chiffrement d'un texte s'obtient en soustrayant une quantité fixe à chaque code.[^1]

[^1]: Pour rester *lisible*, il faut recommencer à code<sub>min</sub> lorsqu'on a dépassé code<sub>max</sub> (et vice-versa)


```python title="Chiffrement de César (26 lettres minuscules, décalage de 3)"
message_en_clair = "cybersecurite"
message_chiffre = '' # vide au départ

for c in message_en_clair: # boucle sur les caractères du message en clair
    code = ord(c)
    nouveau_code = code - 3

    # Pour rester entre 97 et 122
    if nouveau_code < 97:
        depassement = 97 - nouveau_code
        nouveau_code = 122 - depassement + 1

    # Construction du message chiffré
    message_chiffre = message_chiffre + chr(nouveau_code)

print(message_chiffre)
```

## 3. Déchiffrement <a name="dechiffrement"></a>
    
Il s'agit de l'opération inverse de `chiffrement`.

```python title="Déchiffrement de César (26 lettres minuscules, décalage de 3)"
message_chiffre = 'sfwfiib'
message_dechiffre = '' # vide au départ

for c in message_chiffre:
    code = ord(c)
    nouveau_code = code + 3

    # Pour rester entre 97 et 122
    if nouveau_code > 122:
        depassement = nouveau_code - 122
        nouveau_code = 97 + depassement - 1

    message_dechiffre = message_dechiffre + chr(nouveau_code)

print(message_dechiffre)
```

## 4. Décryptage

Ici, la connaissance de la clé de **chiffrement** permet de réaliser directement le **déchiffrement**: on parle de **chiffrement symétrique**.

Il existe 2 principales attaques sur ce type de chiffrement :

- attaque par **force-brute**: on essaye toutes les clés possibles jusqu'à obtenir un message déchiffré *lisible*.
- attaque **statistique**: on identifie le caractère qui apparaît le plus souvent dans le message chiffré et on fait l'hypothèse qu'il s'agit d'un `e` *décalé* (ou tout autre caractère apparaissant souvent dans un texte).
---
hide:
  - navigation
---

# Mot de passe

??? tip inline end "Modes d'authentification"

    Selon les cas, l'utilisateur peut s'authentifier par:

    - ce qu'il sait (mot de passe...),
    - ce qu'il détient (clé physique...),
    - ce qu'il est (donnée biométrique).

    Les experts en sécurité préconisent souvent d'utiliser au moins deux formes d'authentification (*authentification multi-facteur*) pour les comptes importants.

Il s'agit d'un des modes d'authentification d'un utilisateur.

<figure markdown>
![Manipulation mot de passe](img/src/01-manip-mdp.svg)
</figure>

Le système d'authentification ne manipule pas directement le mot de passe, mais son **condensat** (*hash*, empreinte numérique) obtenu par une fonction de hachage.

??? danger "Chiffrement/hachage"

    Pour des raisons de sécurité, le système d'authentification ne doit conserver aucune trace des mots de place en clair:  c'est pour cela que les mots de passes ne sont pas *chiffrés* (action réversible) mais *hachés* (action non réversible). 

## 1. Fonction de hachage

C'est une fonction mathématique unidirectionnelle (facilement calculable, mais difficilement réversible[^2]).

[^2]: Les fonctions de hachage sont aussi utilisées de vérifier l'intégrité de données, détecter des doublons, signer des documents numériques, valider une *blockchain*...

<figure markdown>
![Fonction de hachage](img/src/01-hachage_mdp.svg){ width="700" }
</figure>

- Un mot de passe de longueur quelconque en entrée produit un *condensat* (ou *hash*[^3]) de longueur fixe.

    [^3]: ou *empreinte numérique* ou *digest* ou *checksum* ou *somme de controle*

    <figure markdown>
    ![Longueur de condensat](img/src/01-hachage_mdp-longueur.svg){ width="400" }
    </figure>

- La moindre modification sur le mot de passe d'entrée produit un condensat très différent.

    <figure markdown>
    ![Variabilité du l'empreinte](img/src/01-hachage_mdp-modif.svg){ width="400" }
    </figure>

- Il est quasiment impossible (d'un point de vue calculatoire) de trouver une 2 mots de passe produisant le même condensat (=**collision**).

    <figure markdown>
    ![Collisions](img/src/01-hachage_mdp-collision.svg){ width="300" }
    </figure>

??? note "Exemples de fonctions de hachage"

    === "MD5" 

        *(Message Digest 5, Ron Rivest - 1991)*

        - Les empreintes ont une taille de 128 bits (16 octets, 32 chiffres hexadécimaux).
        - Considéré obsolète aujourd'hui (utilisé seulement si aucune autre alternative)

        Utilisations :

        - empreinte de fichier :

            ```console
            $ md5sum <fichier>
            $ openssl md5 <fichier>
            ```

        - empreinte d'une chaîne de caractères (ex : mot de passe) :

            ```console
            $ echo -n "Ma chaine de caractères" | md5sum
            e7dee73197cd8e96a27e040633f597a5
            $ echo -n "Ma chaine de caractères" | openssl md5
            e7dee73197cd8e96a27e040633f597a5
            ```

    === "SHA"

        *(Secure Hash Algorithm)*

        - Suite d'algorithmes développés par la NSA

            - SHA-1: en 1995, plusieurs versions, 160 bits (20 octets, 40 chiffres hexa), obsolète aujourd'hui, plus lent que MD5
            - SHA-2: SHA-224 (224 bits), SHA-256 (256 bits), SHA-384 (384 bits), SHA-512 (512 bits).
            - SHA-3 (créé par NIST): alternative/remplacement? de SHA-2 avec SHA3-224 (224 bits), SHA3-256 (256 bits), SHA3-384 (384 bits), SHA3-512 (512 bits).

        *[NSA]: National Security Agency

        Utilisations :

        - empreinte de fichier :

            ```console
            $ sha1sum <fichier>

            $ sha224sum <fichier>
            $ sha256sum <fichier>
            $ sha384sum <fichier>
            $ sha512sum <fichier>

            $ openssl sha1 <fichier>
            $ openssl sha224 <fichier>
            $ openssl sha256 <fichier>
            $ openssl sha384 <fichier>
            $ openssl sha512 <fichier>

            $ openssl sha3-224 <fichier>
            $ openssl sha3-256 <fichier>
            $ openssl sha3-384 <fichier>
            $ openssl sha3-512 <fichier>
            ```

    ??? tip "Identification du type d'un condensat"

        L'utilitaire Python `hashid` permet d'indentifier le type de condensats (parmis 210 supportés).

## 2. Attaques directes et parades

=== "Attaque par force brute"

    (*brute-force attack*)

    ![Attaque par force brute](img/src/01-hachage-attaque_bruteforce.svg){ width="60%" align="left" }
    
    On essaye toutes les combinaisons possibles (selon l'alphabet utilisé), jusqu'à trouver le condensat cible.

    Cette attaque aboutit toujours mais pas nécessairement dans un temps *raisonnable*.

    ??? tip "Parade"
    
        Utiliser des alphabets très grands (majuscules, minuscules, chiffres, caractères spéciaux...) et/ou des mots de passe très longs (*phrases de passe*...).

=== "Attaque par dictionnaire"

    (*dictionnary/wordlist attack*)

    ![Attaque par dictionnaire](img/src/01-hachage-attaque_dictionnaire.svg){ width="60%" align="left" }

    On esssaye tous les mots de passe connus (et conservés dans un *très gros* fichier), jusqu'à trouver le condensat cible. 
    
    - Le même dictionnaire peut être réutilisé avec plusieurs fonctions de hachage.
    - La durée de l'attaque dépend de la taille du dictionnaire et de la rapidité de calcul de la fonction de hachage.
    
    Cette attaque n'aboutit que si le mot de passe recherché est connu (=présent dans le dictionnaire).

    ??? tip "Parades" 
    
        - Utiliser des mots de passe originaux (+ insérer volontairement des fautes d'orthographe)
        - Ne pas les réutiliser à plusieurs endroits (risque de brêches sur des systèmes mal protégés). 
        - Le système peut aussi recourir à des fonctions de hachage *moins rapides*.

=== "Attaque par tables arc-en-ciel"

    (*rainbow tables attack*)

    ![Attaque par tables arc-en-ciel](img/src/01-hachage-attaque_rainbow_table.svg){ width="60%" align="left" }

    On recherche le condensat directement dans la table arc-en-ciel. 
    
    ??? tip "Quelques précisions"

        - La génération de la table est très longue : elle est réalisée en amont à l'aide d'un dictionnaire.
        - Elle ne sert que pour une seule fonctions de hachage.
        - La fonction de hachage ne sert pas durant l'attaque.

    Cette attaque est extrêmement rapide mais, comme pour l'attaque par dictionnaire, elle n'aboutit que si le hash du mot de passe recherché est présent dans la table (et donc si le mot de passe est connu).

    ??? tip "Parade"
    
        Utiliser des fonctions de hachage *salées* (Cf. après).

??? success "Recommandations pour un mot de passe *robuste*"

    (voir l'[article de la CNIL](https://www.cnil.fr/fr/mots-de-passe-une-nouvelle-recommandation-pour-maitriser-sa-securite))

    - Ne pas l'écrire (et surtout pas à proximité du système)
    - Ne pas le réutiliser (utiliser un gestionnaire).
    - Longueur : au minimum 12 caractères.
    - Complexité : combinaison de majuscules, minuscules, chiffres, symboles et espaces (si permis).
    - Ajout volontaire de fautes d'orthographe.
    - Renouvellement régulier (avec un gestionnaire).

## 3. Fonction de hachage salée

(*salted hash function*)

L'idée consiste à *combiner* une valeur aléatoire (=sel ou **salt**) avec le mot de passe durant le hachage.

<figure markdown>
![Hachage salé](img/src/01-hachage_salt.svg){ width="400" }
</figure>

ainsi chaque ré-hachage d'un même mot de passe génère un condensat différent. 

<figure markdown>
![Hachage salé](img/src/01-hachage_salt2.svg){ width="500" }
</figure>

et il n'est plus possible de pré-calculer tous les condensats dans une table arc-en-ciel (il faudrait le faire pour toutes les valeurs aléatoires possibles).

??? tip "Contraintes de stockage" 

    Le système d'authentification doit nécessairement connaître le sel utilisé pour valider l'utilisateur. Les enregistrements de mots de passe contiennent alors 3 ou 4 champs (séparés ici par `$`):

    ```
    Format 1: $sel$condensat
    Format 2: $idAlgo$sel$condensat
    Format 3: $idAlgo$paramètres$sel$condensat
    ```

??? note "Exemples de fonctions de hachage salées"

    === "md5crypt"

        Appelé MD5 version BSD. Anciennement utilisé sous Linux.

        ```bash
        # Format: $1$sel$condensat
        $1$mERr$cCJigWu8aUIURkJFSgr0A.
        ```

        - Le sel est représenté en ASCII (ici 4 caractères, mais peut faire jusqu'à 12 octets)
        - Le condensat est représenté en base64.

        Exemple de mise en œuvre interactive (valeur `salt` choisie aléatoirement) :

        ```console
        $ mkpasswd --method=md5crypt 
        Mot de passe : 
        ```

    === "yescrypt"

        Méthode actuelle sous Linux.

        ??? warning inline end "Remarque"

            `j9T` correspond aux paramètres par défaut de `yescrypt`.

        ```bash
        # $y$parametres$sel$condensat
        $y$j9T$4afuRIrRSwrPvkuWHviau1$Xvv03VJCgMJ4Ymh2RbcU37Dal3mvB7p9YIopzv6ZZi2
        ```

        Exemple de mise en œuvre intéractive :

        ```console
        $ mkpasswd --method=yescrypt
        Mot de passe : 
        ```


## 4. Condensats et OS

Les systèmes utilisent maintenant presque tous des fonctions de hachage salées.

=== "Linux"

    Les condensats d'utilisateurs locaux sont stockés dans le fichier `/etc/shadow`, uniquement lisible par `root`.

    Principaux identifiants de hachage :

    - `1` = MD5
    - `2a` ou `2y` = Blowfish
    - `5` = SHA-256
    - `6` = SHA-512
    - `y` = yescrypt (méthode actuelle par défaut)

=== "Windows"

    - LM : *Lan Manager* hashes (obsolète aujourd'hui)
    
        Utilisé sur Windows XP et Windows serveur 2003, mots de passe jusqu'à 14 lettres affichables et insensibles à la casse (=64 caractères possibles par lettre) :
        
        - Conversion en majuscules
        - Complétion à 14 caractères avec des `0x0`.
        - Découpage en 2 clés de 7 octets pour DES

        ??? warning "`aad3b435b51404eeaad3b435b51404ee`"

            Ce condensat indique un mot de passe vide.

    - NTLM : remplace actuellement LM sur Windows (depuis Windows NT) pour les comptes locaux.

        Mots de passes sensibles à la casse.

    - DCC : Domain Cache Credential
    - DCC2

## 5. Outils pour craquer

=== "Hashcat" 

    ??? tip "Installation sous Fedora"

        ```console
        $ sudo dnf install hashcat
        ```

    [Utilitaire](https://github.com/hashcat/hashcat) avec 6 modes d'attaque pour 300 algorithmes de hachage. Utilise CPU et GPU. Fonctionne sous Linux, Windows, macOS.

    Exemple : attaque par force-brute avec masque sur un condensat LM

    ```bash
    hashcat -a 3 #(1)! \
            -m 3000 #(2)! \
            d3bf255c530633b9aad3b435b51404ee #(3)! \
            ?u?u?u?u?u?u?u #(4)!
    ```

    1. Type d'attaque

        - `0` : directe (dictionnaire)
        - `1` : Combinée
        - `3` : Force-brute
        - `6` : Hybride: dictionnaire + masque
        - `7` : Hybride: masque + dictionnaire
        - `9` : Association

        `hashcat -h` pour plus d'informations

    2. Type de condensat

        - `0`: MD5
        - `3000` : LM

        `hashcat -h` pour plus d'information

    3. Condensat ou nom du fichier contenant le(s) condensat(s) à craquer
    4. Masque

=== "John the Ripper"

    ??? tip "Installation sous Fedora"

        ```
        sudo dnf install john
        ```

        (version très incomplète, il est préférable d'installer la version *jumbo* depuis github)

        ```
        git clone https://github.com/openwall/john.git
        cd john/src/
        ./configure && make
        ```
    
    424 formats de hachage supportés.

    Exemple : attaque par force-brute avec masque sur un condensat de type LM

    ```bash
    /usr/local/JtR/run/john lmhash.txt #(1)!
            --format=LM #(2)! \
            "-mask=?a?a?a?a?a?a?a" #(3)!
    ```

    1. Nom du fichier contenant le(s) condensat(s)
    2. Méthode de hachage

        `/usr/local/JtR/run/john --list=formats` pour voir les méthodes supportés.

    3. Masque pour l'attaque

=== "Ophcrack"

    [Ophcrack](https://ophcrack.sourceforge.io/): mots de passe Windows, gratuit, outil Windows ou Linux, GUI, basé sur des tables arc-en-ciel (rainbow tables)

    ```console
    sudo dnf install ophcrack
    ```

=== "L0phtCrackL" 

    Outil Windows, bibliothèques commerciales, dictionnaire/brute-force/hybride/rainbow tables

=== "THC Hydra"

    Attaque en ligne de commande par force brute ou dictionnaire via plus de 50 protocoles, notamment de connexion (telnet, HTTP, HTTPS, SMB...).

    - Installation :

        ```console
        sudo dnf install hydra
        ```

    - Exemple d'utilisation:

        ```console
        hydra -l admin -P rockyou.txt -e nrs -vv challenge01.root-me.org http-get /web-serveur/ch3/
        ```

        - `-l`: nom de connexion (login)
        - `-P`: dictionnaire de mots de passe
        - `-e nrs`: tester le mot de passe vide et le mot de passe égal au login et son inverse.
        - `-v`: mode verbose

=== "RainbowCrack" 

    [RainbowCrack](http://project-rainbowcrack.com/): utilise des tables arc-en-ciel.

    ```
    sudo snap install rainbowcrack --beta
    ```

=== "Medusa"

    ```
    sudo dnf install medusa
    ```


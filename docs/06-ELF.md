---
hide:
  - navigation
---
# Programme exécutable Linux

??? tip inline end "Format de programme"

    L'utilisation d'un format permet de segmenter le programme en plusieurs sections selon leur usage :

    - Le code en langage machine (lecture et en exécution)
    - Les données telles que les constantes (lecture)
    - Les variables globales et les données non initialisées (lecture et écriture)
    - &hellip;

    Ce format fournit aussi des informations importantes au système :

    - Type d'exécutable.
    - Architecture cible.
    - Point d'entrée du programme (adresse début à exécuter).
    - &hellip;

Le noyau Linux prend en charge le format de fichier ELF.[^1]

*[ELF]: Executable and Linkable Format
[^1]: Windows utilise le format PE (Portable Executable) et Mac OS X le format Mach-O (Mach-object).

## 1. Espace d'adressage

C'est le nom donné à l'organisation de la mémoire mise à disposition du processus.

!!! info "Exemple"

    ![Espace d'adressage](img/06-espace_adressage.svg){ width=40% }

En raison de la virtualisation de la mémoire, chaque processus dispose d'un 
espace d'adressage qui lui est propre et qui ne rentre pas en conflit avec 
celui des autres processus.

## 2. Informations sur fichier ELF

avec l'utilitaire `readelf`

??? info inline end "Détails"

    Il s'agit ici d'un programme exécutable de type `DYN` destiné à une architecture `X86-64` et le point d'entrée est fixé à l'adresse `0x2b90`.

```{.console hl_lines="9 10 12" .rgconsole title=Terminal}
[cyber@localhost ~]$ readelf -a /bin/sleep
En-tête ELF:
Magique:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00 
Classe:                            ELF64
Données:                          complément à 2, système à octets de poids faible d'abord (little endian)
Version:                           1 (actuelle)
OS/ABI:                            UNIX - System V
Version ABI:                       0
Type:                              DYN (fichier objet partagé)
Machine:                           Advanced Micro Devices X86-64
Version:                           0x1
Adresse du point d'entrée:         0x2b90
Début des en-têtes de programme :  64 (octets dans le fichier)
Début des en-têtes de section :    35056 (octets dans le fichier)
Fanions:                           0x0
Taille de cet en-tête:             64 (octets)
Taille de l'en-tête du programme:  56 (octets)
Nombre d'en-tête du programme:     13
Taille des en-têtes de section:    64 (octets)
Nombre d'en-têtes de section:      31
Table d'index des chaînes d'en-tête de section: 30
...
```

??? tip inline end "Détails"

    On apprend ici que le code en langage machine (section `[15].text` située à 
    un décalage `0x25f0`) occupe une taille de `0x2b62` octets et qu'il doit 
    être positionné en mémoire à l'adresse `0x25f0` (réallouable).


```{.console hl_lines="25 26" .rgconsole}
...
En-têtes de section :
    [Nr] Nom               Type             Adresse           Décalage
        Taille            TaillEntrée      Fanion Lien  Info  Alignement
    ...
    [ 6] .dynsym           DYNSYM           00000000000003d0  000003d0
        0000000000000588  0000000000000018   A       7     1     8
    [ 7] .dynstr           STRTAB           0000000000000958  00000958
        00000000000002b3  0000000000000000   A       0     0     1
    [ 8] .gnu.version      VERSYM           0000000000000c0c  00000c0c
        0000000000000076  0000000000000002   A       6     0     2
    [ 9] .gnu.version_r    VERNEED          0000000000000c88  00000c88
        0000000000000050  0000000000000000   A       7     1     8
    [10] .rela.dyn         RELA             0000000000000cd8  00000cd8
        00000000000001b0  0000000000000018   A       6     0     8
    [11] .rela.plt         RELA             0000000000000e88  00000e88
        0000000000000450  0000000000000018  AI       6    24     8
    [12] .init             PROGBITS         0000000000002000  00002000
        000000000000001b  0000000000000000  AX       0     0     4
    [13] .plt              PROGBITS         0000000000002020  00002020
        00000000000002f0  0000000000000010  AX       0     0     16
    [14] .plt.sec          PROGBITS         0000000000002310  00002310
        00000000000002e0  0000000000000010  AX       0     0     16
    [15] .text             PROGBITS         00000000000025f0  000025f0
        0000000000002b62  0000000000000000  AX       0     0     16
    [16] .fini             PROGBITS         0000000000005154  00005154
        000000000000000d  0000000000000000  AX       0     0     4
    [17] .rodata           PROGBITS         0000000000006000  00006000
        0000000000000a60  0000000000000000   A       0     0     32
    ...
```

## 3. Désassemblage

Avec la commande `objdump`:

```{.console .rgconsole  title=Terminal}
[cyber@localhost ~]$ objdump -d /bin/sleep

Déassemblage de la section .init :

0000000000002000 <.init>:
    2000:       f3 0f 1e fa             endbr64
    2004:       48 83 ec 08             sub    $0x8,%rsp
    2008:       48 8b 05 a1 5f 00 00    mov    0x5fa1(%rip),%rax        # 7fb0 <__ctype_b_loc@plt+0x59f0>
    200f:       48 85 c0                test   %rax,%rax
    2012:       74 02                   je     2016 <free@plt-0x2ea>
    2014:       ff d0                   call   *%rax
    2016:       48 83 c4 08             add    $0x8,%rsp
    201a:       c3                      ret
...
```

## 4. Débogage

??? tip inline end "Raccourcis de commandes"

    Les commandes `gdb` peuvent être raccourcis lorsqu'il n'y a pas d'ambiguité.

    | Commande | Raccourci |
    |:--------:|:----------|
    | `break`  | `b`       |
    | `step`   | `s`       |
    | `next`   | `n`       |

Avec l'utilitaire `gdb`

- Lancement

    ```{.console .rgconsole .rgconsole title=Terminal}
    [cyber@localhost ~]$ gdb executable
    ```
    ou
    ```{.console .rgconsole}
    [cyber@localhost ~]$ gdb -q executable
    ```

- Mettre un point d'arrêt:

    - au début d'une fonction `main` :

        ```{.console .rgconsole}
        (gdb) break main
        ```

    - à une adresse précise:

        ```{.console .rgconsole}
        (gdb) break *0x08048700
        ```

- Gérer l'exécution:

    - Démarrer :

        ```{.console .rgconsole}
        (gdb) run
        ```

    - Redémarrer :

        ```{.console .rgconsole}
        (gdb) continue
        ```

    - Avancer pas à pas:


        ??? tip inline end "variante"

            Avec affichage de l'instruction exécutée

            ```{.console .rgconsole}
            (gdb) next
            ```

        ```{.console .rgconsole}
        (gdb) step
        ```

- Désassembler une fonction: 

    ```{.console .rgconsole}
    (gdb) disassemble main
    ```

- Afficher du contenu de mémoire: `x` ou `x/format` ou `x/[nb_elements][format]`

    ??? tip "Formats"

        | Format | Signification | 
        |:------:|:--------------|
        | `x`    | hexadécimal   |
        | `d`    | Décimal signé |
        | `u`    | Non signé     |
        | `t`    | Binaire       |
        | `f`    | Flottant      |
        | `a`    | Adresse       |
        | `c`    | Caractère     |
        | `s`    | Chaîne        |
        | `i`    | Instruction   |

    ??? tip "Modificateurs de taille"

        | Symbole | Signification        |
        |:-------:|:---------------------|
        | `b`     | 8 bits (octet)       |
        | `h`     | 16 bits (half-word)  | 
        | `w`     | 32 bits (word)       |
        | `g`     | 64 bits (giant word) |

    - Contenu de registres :
    
        (premier et deuxième argument de la pile d'appel)

        ```{.console .rgconsole}
        (gdb) x/1xw $esp
        (gdb) x/1xw $esp+4
        ```

    - Les 5 prochaines instructions :
    
        ```{.console .rgconsole}
        (gdb) x/5i $pc
        ```
    - Chaîne de caractères :

        ```{.console .rgconsole}
        (gdb) x/1s 0x0804b008
        (gdb) x/1s 0x08048841
        ```

- Modifier du code: 

    (*remplacement d'un `jne`(0x75) par un `je`(0x74)*)
    ```{.console .rgconsole}
    (gdb) set {char}0x08048707=0x74
    ```


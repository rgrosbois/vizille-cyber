---
hide:
  - navigation
---
# Assembleur x86-64

Adressage et instructions sur 64 bits, mode de compatibilité avec `x86` (32 bits).

- Assemblage en module objet (`as`):

    ```console
    as -o fichier.o fichier.s
    ```

    avec affichage de l'assemblage:
    ```console
    as -a -o fichier.o fichier.s
    ```

    avec l'insertion de points d'arrêt pour le débugger (exemple `ddd`):
    ```console
    as -a --gstabs -o fichier.o fichier.s
    ```

- Édition de lien (`ld`):

    ```console
    ld [-e <symbole>] <fichier1> <fichier2> ... <fichierN> -o <exécutable>
    ```

## 1. Registres

16 registres généraux (64 bits):

- `rax` (accumulateur)  = eax: 32 bits poids faible/ax 16 bits poids faible=ah + al, 
- `rcx` (compteur) =  ecx: 32 bits poids faible,
- `rdx` (données) = edx: 32 bits poids faible,
- `rbx` (base) = ebx: 32 bits poids faible,
- `rsp` (sommet de pile) = esp: 32 bits poids faible,
- `rbp` (base de pile) = ebp: 32 bits poids faible,
- `rsi` (source index) = esi: 32 bits poids faible,
- `rdi` (destination index) = edi: 32 bits poids faible,
- `r8`, 
- `r9`, 
- `r10`, 
- `r11`, 
- `r12`, 
- `r13`, 
- `r14`, 
- `r15`

Registres de base de segments (segments partitionnaient la mémoire quand manque d'adresse en 32/16 bits): Windows/MacOS/Linux les font pointer dorénavant vers la même adresse (*flat memory model*)

- SS (stack segment)
- CS (code segment)
- DS (data segment), ES (extra segment), FS et GS.

Registre d'état (seuls 32 bits utilisés):

- CF: Carry Flag (bit 0)
- ZF: Zero Flag (bit 6)
- SF: Sign Flag (bit 7)
- DF: Direction Flag (bit 10)
- OF: Overflow Flag (bit 11)

## 2. Modes d'adressage

- Adressage immédiat: `$97` ou `$0x61`.
- Par registre: `%rax`
- Par adresse mémoire :

    - étiquette du programme (adressage direct)
    - adresse d'une donnée: `$compteur` (adressage indirect)
    - référence à la mémoire: `déplacement(base, index, multiplicateur)` signifie `base+index*multiplicateur+déplacement`.

        - `(%rcx)`: mémoire dont l'adresse est dans rcx (adressage indirect)
        - `(%rcx, %rax)`: mémoire dont l'adresse est rcx+rax (adressage indirect indexé) 
        - `-8(%rbp)`: 8 octets avant rbp
        base et index peuvent être des registres, déplacement et multiplicateur sont des constantes.

## 3. Instructions

Types (format `as`):

- `instr`
- `instr arg`
- `instr source, destination`
- `instr aux, source, destination`

La taille des arguments est donnée par la dernière lettre:

- `b`: octet (8 bits)
- `w`: mot (16 bits)
- `l`: long (32 bits)
- `q`: quad (64 bits)

### a. Arithmétiques

- `Op src, dst` signifie `dst = dst Op src`
- `Op arg` signifie que le 2ème opérande est rax.

| Mnémonic | Utilisation  | Signification               |
|:---------|:------------:|:----------------------------|
| add(adc) | add src, dst | Addition (avec retenue)     |
| sub(sbb) | sub src, dst | Soustraction (avec retenue) |
| mul      | mul arg      | Multiplication              |
| div      | div arg      | Division                    |
| sal      | sal n,arg    | Décalage arith. à gauche    |
| sar      | sar n,arg    | Décalage arith. à droite    |
| neg      | neg arg      | Remplace par opposé         |
| inc      | inc arg      | Remplace par incrément      |
| dec      | dec arg      | Remplace par décrément      |

### b. Logiques

| Mnémonic | Utilisation  | Signification               |
|:---------|:------------:|:----------------------------|
| and      | and src, dst | Et logique                  |
| or       | or src, dst  | Ou logique                  |
| xor      | xor src, dst | Ou-exclusif logique         |
| not      | not arg      | Inverse logique             |
| shl      | sal n,arg    | Décalage logique à gauche   |
| shr      | sar n,arg    | Décalage logique à droite   |

### c. Transfert

| Mnémonic | Utilisation  | Signification               |
|:---------|:------------:|:----------------------------|
| mov      | mov src, dst | Affecte src à dst           |
| push     | push src     | Empile src (màj de rsp)     |
| pop      | push dst     | Dépile dans dst (màj de rsp)|

### d. Comparaisons

| Mnémonic | Utilisation     | Signification               |
|:---------|:---------------:|:----------------------------|
| test     | test arg1, arg2 | calcule le ET des 2 args    |
| cmp      | cmp arg1, arg2  | soustrait arg1 de arg2      |

### e. Sauts

| Mnémonic | Signification       |
|:---------|:--------------------|
| JMP adr  | Saut inconditionnel |
| JE adr   | Saut si égalité     |
| JNE adr  | Saut si non-égalité |
| JZ adr   | Saut si zéro        |
| JNZ adr  | Saut si non-zéro    |
| JL adr   | Saut si arg2<arg1   |

(il existe aussi JG, JLE, JGE)

### f. Sous-programmes

- appel avec `call adr` (rip est empilé)
- rend la main avec `ret` (rip est dépilé)

### g. Appels système

Pour un appel système: le système a sa propre pile, les registres sont inchangés (sauf peut-être rcx et r11)

- numéro de l'appel placé dans rax (`exit`: 60, `write`: 1)
- paramètres dans l'ordre: rdi, rsi, rdx, r10, r8 et r9.
- appel avec `syscall`
- valeur de retour dans `rax`

Exemples:

- pour exit:

    ```asm
    movq $60, %rax  # numéro de exit
    xorq %rdi, %rdi # rdi <- 0
    syscall
    ```

- pour write (`int write(int fd, void *buf, size_t longueur)`)

    ```asm
    movq $1, %rax # numéro de write
    movq $1, %rdi # stdout
    movq $message, %rsi # adresse 1er octet
    movq $23, %rdx # longueur de chaine
    syscall
    ```

## 4. Argument ligne de commande

Dès le lancement du programme, la pile d'exécution contient :

- argc
- argv[0]
- argv[1]
- ...
- argv[arc-1]
- 0

## 5. Exemple de programme

```asm
.data # début du segment de données

tableau:
    .quad 1
    .quad 5
    .quad 2
    .quad 18

somme:
    .quad 0

.text # début du segment de code

.global _start
_start:
    xor %rax, %rax # rax = 0
    xor %rbx, %rbx # rbx = 0
    mov $tableau, %rcx # RCX = tableau

tant_que:
    cmp $4, %rax
    je fin
    add (%rcx, %rax, 8), %rbx # RBX = RCX[RAX]
    inc %rax
    jmp tant_que

fin:
    mov %rbx, somme  # fini, résultat dans somme
    mov %60, %rax
    xor %rdi, %rdi
    syscall
```

---
hide:
  - navigation
---

# Stockage sur mémoire de masse

Une mémoire de masse est un support de stockage de grande capacité, non volatile, pouvant être lue et écrite par un ordinateur.

## 1. Partitionnement

Partionner un disque consiste à créer des divisions (=partitions) afin de gérer les informations de manière séparée[^1]. 

<figure markdown>
![Partitionnement](img/src/02-partitionnement.svg)
</figure>

??? tip inline end "Partitions et OS"

    Un système d'exploitation peut avoir besoin d'une ou plusieurs partitions pour fonctionner:

    - Sous Linux, il est traditionnel d'avoir au moins les partitions: *boot* (<tt>/boot</tt>), *racine* (<tt>/</tt>)...
    - Depuis Windows 7 (?), il y a, au minimum, une partition pour le système et une autre pour les données.
    - Il existe parfois des partitions de restauration du système (en début ou fin de disque).


Les adresses de début et fin de chaque *partition* sont répertoriées dans la **table de partition** du disque selon l'un des 2 formats suivants :

- CHS (historique, sur disque dur): numéros du cylindre (C), de la tête de lecture/écriture (H) et du secteur utilisé (S). Le 1er secteur a pour adresse *CHS=0/0/1*.
- LBA (plus récent): numérotation des *N* secteurs[^4] de *0* à *N-1*. Le premier secteur a pour adresse *LBA 0*.

*[CHS]: Cylinder/Head/Sector (stockages jusqu'à 2 Go)
*[LBA]: Logical Block Addressing (numérotation des secteurs)
[^4]: Les secteurs faisaient traditionnellement 512 octets. Mais on trouve dorénavant des secteurs de 1024 octets, 2048 octets, 4096 octets...

[^1]: La gestion s'effectue par un (ou plusieurs) système(s) d'exploitation via un système de fichiers.

Les 2 principaux types de partitionnement sont MBR et GPT.

*[MBR]: Master Boot Record
*[GPT]: GUID Partition Table

=== "MBR"

    *(David Litton, IBM, 1982)*

    Sur les architectures anciennes (PC/Intel) pour des capacités de stockage inférieures à 2 To[^2].

    [^2]: La limite de 2 To correspond à des secteurs de 512 octets avec un adressage 32 bits.

    Le nom[^3] vient du premier secteur qui contient la table de partition et une routine d'*amorçage* du disque.

    [^3]: Zone d'amorce ou enregistrement d'amorçage maître (MBR)


    Sa table de partition peut utiliser des adresses CHS ou LBA et peut contenir jusqu'à :

    - 4 partitions *primaires* qui peuvent contenir un bootloader et donc démarrer un système d'exploitation.
    - 1 partition *étendue* pouvant être divisée en plusieurs partitions logiques.

=== "GPT"

    Pour des architectures plus récentes (depuis 2010) supportant des capacités supérieures à 2 To. L'adressage est uniquement de type LBA.

    - LBA 0 (MBR protecteur) : rétrocompatibilité MBR + programme de démarrage (*bootloader*) de systèmes BIOS (non-EFI).
    - LBA 1: en-tête GPT (primaire)
    - LBA 2: table de partitionnement (redondance à la fin du disque)
    - ...
    - LBA 34: premier secteur utilisable (sous Windows 64 bits)
    - ...
    - LBA -2: table de partitionnement redondante
    - LBA -1: en-tête GPT redondant (secondaire)

## 2. Systèmes de fichiers

Chaque partition peut avoir son propre système d'organisation des fichiers, défini au moment du *formatage*.

Exemples:

- Fat16/Fat32 (sous Dos)
- NTFS (sous Windows)
- ext3/ext4/btrfs (sous Linux)
- ...
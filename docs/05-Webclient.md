---
hide:
  - navigation
---

# Clients Web

## 1. Ligne de commande

### 1.1 curl

Cet outil permet de transférer des données depuis ou vers un serveur sans interaction avec l'utilisateur. Il supporte les protocoles: HTTP(S), IMAP(S), LDAP(S), MQTT, POP3(S), RTMP(S), RTSP, SCP, SFTP, SMB(S), SMTP(S), TELNET, TFTP, WS(S).

Exemples d'utilisation :

- Télécharger une page Web dans la console :

    ```console
    curl http://challenge01.root-me.org/web-serveur/ch1/
    ```

??? tip "Options utiles"

    - `-o`: pour enregistrer la réponse dans un fichier
    - `-G` pour envoyer avec `GET` des données spécifiées avec l'option `-d`

        ```console
        curl -G http://challenge01.root-me.org/web-serveur/ch52/ -d "url=https://google.fr&h=0edf27c83d4aa4699c0625d27be0e371"
        ```


### 1.2. wget

Téléchargement non interactif de fichiers avec les protocols HTTP(s), FTP et à travers des proxy Web. La sauvegarde s'effectue, par défaut, dans un fichier qui porte le même nom que la ressource.

Exemples d'utilisation :

- Téléchargement classique d'une page dans un fichier :

    ```console
    wget http://challenge01.root-me.org/web-serveur/ch1/
    ```

    *(ici le résultat est sauvegardé dans index.html)*

??? tip "Options utiles"

    - `-S`: affiche la réponse du serveur dans la console

    - `-O -`: pas de sauvegarde mais affichage dans la console

    - `-U`: pour spécifier le *user-agent*

        ```console
        wget -U 'admin' -O - http://challenge01.root-me.org//web-serveur/ch2/
        ```

        *(ici on a `User-agent: Admin`)*

    - Ajout d'un en-tête supplémentaire:

        ```console
        wget http://challenge01.root-me.org/web-serveur/ch68/ --header 'X-Forwarded-For: 192.168.1.1'
        ```

## 2. Programmation

### 2.1. Python request

```python
import request

url = 'http://challenge01.root-me.org/web-serveur/ch68/index.php'
reponse = requests.get(
    url, 
    headers = {'Client-IP' : '192.168.1.1'} 
)
     
if reponse.status_code == 200:
    print(reponse.text)
else:
    print('Error')
```

## 3. Applications graphiques

### 3.1. Postman

### 3.2. Burp Suite

## 4. Navigateur

Dans l'outil d'inspection (++ctrl+shift+i++, ou ++f12++, ou clic droit sur un élément graphique et choisir *inspecter l'élément*) :

- Onglet réseau: clic droit sur une requête puis *Modifier et renvoyer* pour éditer/modifier la requête.
- Bouton *Vue adaptative* (icône de smartphone): possibilité de modifier le *user-agent*.

## 5. Scanner de vulnérabilités

### 5.1. nikto

Outil en ligne de commande permettant d'identifier des problèmes ou vulnérabilités telles que :

- mauvaise configuration de serveur ou logiciel
- programmes et fichiers par défaut
- programmes et fichiers non sécurisés
- programmes et serveurs périmés